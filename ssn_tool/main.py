from ssn_tool.utils import *
import itertools
import copy
import sys
import os

'''
class SimState:

    def __init__(self, cp):
        self.ts = 0
        self.nodes = []
'''


def gen_cp_for_CGR_MC_from_net(net,data_rate=sys.maxsize):
    num_of_nodes = net['NUM_OF_NODES']

    #To get identifier corresponding with
    contacts = []
    for id in range(len(net['CONTACTS'])):
        c = net['CONTACTS'][id]
        contacts.append(Contact(c['from'], c['to'], c['ts'], c['ts'] + 1, data_rate,
                        {'capacity': 0, 'arrivalTime': sys.maxsize,
                         'predecessor': None, 'visited': False,
                         'suppressed': False}, id))

    contacts_by_source = {}
    for n in range(num_of_nodes):
        contacts_by_source[n] = []
        for c in contacts:
            if c.source_eid == n:
                contacts_by_source[n].append(c)

    return contacts_by_source


def initial_state(num_of_nodes, cp, traffic):
    return State([copy.deepcopy(cp) for n in range(num_of_nodes)], traffic[0], [], 0)


def make_final_state(s):
    s.limbo += s.incoming_bundles
    s.incoming_bundles = []


def gen_model_for_net(net, routing_algorithm, traffic=None, P=0.5):
    if traffic is None:
        traffic = [net['TRAFFIC']]

    #gen traffic by time stamp
    traffic_by_ts = {}
    number_of_ts = max([c['ts'] for c in net['CONTACTS']]) + 1
    for ts in range(number_of_ts):
        traffic_by_ts[ts] = [Bundle(t['from'], t['to'], 1, t['ts']) for t in traffic if t['ts'] == ts]

    #gen CGR contacts
    cp = CP(gen_cp_for_CGR_MC_from_net(net))

    State.reset_identifier()
    n_of_states = 1
    root = initial_state(net['NUM_OF_NODES'],cp, traffic_by_ts)
    queue = [root]
    while len(queue) > 0:
        if queue[0].ts == number_of_ts:
            break
        current_state = queue.pop(0)
        next_states = compute_next_states(routing_algorithm,current_state,traffic_by_ts,net['NUM_OF_NODES'],P)
        for s in next_states:
            queue.append(s)
            n_of_states += 1

    for s in queue:
        make_final_state(s)

    return (root,n_of_states)


def compute_next_states(routing_algorithm, state, traffic, num_of_nodes, P):

    cps_new = [copy.deepcopy(cp) for cp in state.cps]

    #insert bundles generated by apps in the next timestep
    traffic_new = copy.deepcopy(traffic[state.ts + 1] if (state.ts + 1) in traffic.keys() else [])

    # Contacts id that will be chosen (or have been chosen) by CGR to send bundles from timestamp state.ts to state.ts + 1
    contacts_to_use = []

    routing_decisions = [] # (bundle, route choosed by CGR )
    limbo = copy.copy(state.limbo) # bundles for which no route was found
    delivered_bundles_new = copy.copy(state.delivered_bundles) # bundles which have been already reached the destination
    routed_bundles_new = [] # bundles for which a route in the future was chosen

    for bundle, ts, contact_id in state.routed_bundles:
        if state.ts == ts:
            # If next hop contact will happen in next state, consider it as possible fail link
            contacts_to_use.append(contact_id)
            routing_decisions.append((bundle, contact_id))
        else:
            routed_bundles_new.append((bundle, ts, contact_id))

    for n in range(num_of_nodes):
        for bundle in state.incoming_bundles:
            if bundle.current_node == n: # check if is stored in node n
                selected_route = routing_algorithm.routing({'sim_time': state.ts, 'nodes': cps_new}, bundle)
                #selected_route = cgrForward(cps_new[n], bundle, state.ts)

                if selected_route is not None:
                    first_hop_contact = selected_route.next_hop_contact()
                    # It can't happen to choose a route which first contact happened in the past.
                    if first_hop_contact.s_time < state.ts:
                        print("Error in method compute_next_states: An old route has been chosen.")
                        sys.exit()

                    # If next hop contact will happen in next state, consider it as possible fail link
                    if first_hop_contact.s_time == state.ts:
                        contacts_to_use.append(first_hop_contact.id)
                        routing_decisions.append((bundle, first_hop_contact.id))

                    # Otherwise add it as already routed bundle which has to wait the contact of first hop happen
                    else:
                        routed_bundles_new.append((copy.deepcopy(bundle),
                                                   first_hop_contact.s_time,
                                                   first_hop_contact.id))

                    # Take note of this routing decision in order to implement First Hop Volume Awareness Mechanism
                    #first_hop_contact.residual_volume -= bundle.byte_length
                    routing_algorithm.update({}, selected_route, bundle)
                else:
                    limbo.append(copy.deepcopy(bundle))  # no next hop was selected

    contacts_to_use = set(contacts_to_use) # consider each contact one time
    for n_of_fails in range(0, len(contacts_to_use) + 1):
        for fail_set in itertools.combinations(contacts_to_use, n_of_fails):
            cps_new = [copy.deepcopy(cp) for cp in state.cps]
            traffic_new = copy.deepcopy(traffic[state.ts + 1] if (state.ts + 1) in traffic.keys() else [] )
            delivered_bundles_new = copy.copy(state.delivered_bundles)
            # limbo and routed_bundles will be equal for all childrens
            for bundle, contact_id in routing_decisions:
                # If next hop contact failed then the bundle (a exact copy) will be stored
                # as a traffic_new in order to be send in next state
                if contact_id in fail_set:
                    traffic_new.append(copy.deepcopy(bundle))
                else:
                    # Case in wich bundle can be transmitted by next hop contact
                    first_hop_contact = cps_new[bundle.current_node].get_contact_by_id(contact_id)

                    # Copy bundle to store in next state with the required fields changes
                    new_bundle = copy.deepcopy(bundle)
                    new_bundle.sender_eid = bundle.current_node
                    new_bundle.current_node = first_hop_contact.destination_eid

                    # Check if bundle will be a delivered one in next state
                    if first_hop_contact.destination_eid == bundle.destination_eid:
                        delivered_bundles_new.append(new_bundle)

                    # Else put is as incoming bundle for next node in next state
                    else:
                        traffic_new.append(new_bundle)

                    # Take note of this routing decision in order to implement First Hop Volume Awareness Mechanism
                    first_hop_contact.residual_volume -= bundle.byte_length

            #build next state and store as a state child
            next_state = State(cps_new, traffic_new, copy.deepcopy(routed_bundles_new), state.ts + 1,
                               failed_links=list(fail_set), working_links=[c for c in contacts_to_use if c not in fail_set]  ,limbo=copy.deepcopy(limbo), delivered=delivered_bundles_new)

            state.add_child((P ** len(fail_set) * (1 - P) ** (len(contacts_to_use) - len(fail_set)), next_state))

    return map(lambda e: e[1], state.childs)


def run_analysis_from_file(routing_file_name, routing_name, routing_parameters, path_to_net, working_dir):
    net = get_net_from_file(path_to_net, contact_pf_required=False)
    exec("from ssn_tool.routing.%s import %s" %(routing_file_name, routing_name))
    routing_algorithm = eval("%s(%s)"%(routing_name,routing_parameters))
    res = gen_model_for_net(net,routing_algorithm, P=0.5)

    print(res[0].get_prism_model(net, res[1]))
    print(repr(res[0]))
    print(res[1])

'''
return:
    0 - It was ok
    -1 -
'''
def run_analysis(net, routing_file_name, routing_name, working_dir, traffic=None, routing_parameters=''):
    try:
        exec("from ssn_tool.routing.%s import %s" %(routing_file_name, routing_name))
        routing_algorithm = eval("%s(%s)"%(routing_name,routing_parameters))
        res = gen_model_for_net(net,routing_algorithm, traffic=traffic, P=0.5)
        average_del_ratio_model = res[0].get_prism_model(net, res[1], traffic=traffic)

        f = open(os.path.join(working_dir, 'model-%s.prism'%routing_name) , 'w')
        f.write(average_del_ratio_model)
        f.close()

        return 0
    except Exception as e:
        print(str(e))
        return -1


#from auxiliaryfunctions import get_net_from_file
#net = get_net_from_file('/home/nando/Desktop/SSC/train/net0/net.py',contact_pf_required=False)
#traffic = [{'from':0,'to':3, 'ts':0}, {'from':1,'to':3, 'ts':0},{'from':2,'to':3, 'ts':0}]
#net = get_net_from_file('/home/nando/Desktop/SSC/ssn_tool/examples/case1/c1.py',contact_pf_required=False)
#exec("from ssn_tool.routing.cgr import CGR")
#routing_algorithm = eval("CGR")()
#res = gen_model_for_net(net,routing_algorithm, P=0.5)
#print(res[0].get_prism_model(net, res[1]))
#print(repr(res[0]))
#print(res[1])
#run_analysis('cgr', 'CGR', '', '/home/nando/Desktop/SSC/ssn_tool/examples/case1/c1.py', '')






