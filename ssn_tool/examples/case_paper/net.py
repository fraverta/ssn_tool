NUM_OF_NODES = 4
CONTACTS = [
    {'from': 0, 'to': 1, 'ts': 0, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 0, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 0, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 0, 'pf': 0.1},

    {'from': 0, 'to': 1, 'ts': 1, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 1, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 1, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 1, 'pf': 0.1},

    {'from': 0, 'to': 1, 'ts': 2, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 2, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 2, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 2, 'pf': 0.1},
    {'from': 0, 'to': 3, 'ts': 2, 'pf': 0.5},

    {'from': 0, 'to': 1, 'ts': 3, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 3, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 3, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 3, 'pf': 0.1},

    {'from': 0, 'to': 1, 'ts': 4, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 4, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 4, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 4, 'pf': 0.1},
    {'from': 1, 'to': 3, 'ts': 4, 'pf': 0.5},

    {'from': 0, 'to': 1, 'ts': 5, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 5, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 5, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 5, 'pf': 0.1},

    {'from': 0, 'to': 1, 'ts': 6, 'pf': 0.1},
    {'from': 1, 'to': 0, 'ts': 6, 'pf': 0.1},
    {'from': 1, 'to': 2, 'ts': 6, 'pf': 0.1},
    {'from': 2, 'to': 1, 'ts': 6, 'pf': 0.1},
    {'from': 2, 'to': 3, 'ts': 6, 'pf': 0.5},
]

TRAFFIC = [
            {'from':0, 'to':3, 'ts':0},
            {'from': 1, 'to': 3, 'ts': 0},
            {'from': 2, 'to': 3, 'ts': 0},
        ]