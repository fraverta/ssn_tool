# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/MainWindow.ui'
#
# Created: Wed Oct 10 17:22:39 2018
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ssn_tool.gui.trafficDialog import *
from ssn_tool.gui.traffic_percentage_dialog import *
import ssn_tool.routing.routing_setting
import os
from tempfile import NamedTemporaryFile, mkdtemp
import shutil
import errno


CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.setEnabled(True)
        MainWindow.setFixedSize(633, 605)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.lab_traffic = QtGui.QLabel(self.centralwidget)
        self.lab_traffic.setGeometry(QtCore.QRect(50, 100, 66, 81))
        self.lab_traffic.setObjectName(_fromUtf8("lab_traffic"))
        self.list_traffic = QtGui.QListWidget(self.centralwidget)
        self.list_traffic.setGeometry(QtCore.QRect(140, 100, 351, 81))
        self.list_traffic.setObjectName(_fromUtf8("list_traffic"))
        self.button_add_traffic = QtGui.QPushButton(self.centralwidget)
        self.button_add_traffic.setGeometry(QtCore.QRect(510, 110, 98, 27))
        self.button_add_traffic.setObjectName(_fromUtf8("button_add_traffic"))
        self.button_remove_traffic = QtGui.QPushButton(self.centralwidget)
        self.button_remove_traffic.setGeometry(QtCore.QRect(510, 140, 98, 27))
        self.button_remove_traffic.setObjectName(_fromUtf8("button_remove_traffic"))
        self.button_gen_model = QtGui.QPushButton(self.centralwidget)
        self.button_gen_model.setGeometry(QtCore.QRect(250, 310, 141, 41))
        self.button_gen_model.setObjectName(_fromUtf8("button_gen_model"))
        self.textbox_console = QtGui.QTextEdit(self.centralwidget)
        self.textbox_console.setReadOnly(True)
        self.textbox_console.setGeometry(QtCore.QRect(70, 380, 541, 161))
        self.textbox_console.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.textbox_console.setObjectName(_fromUtf8("textbox_console"))
        self.button_clear_console = QtGui.QPushButton(self.centralwidget)
        self.button_clear_console.setGeometry(QtCore.QRect(20, 400, 41, 31))
        self.button_clear_console.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(os.path.join(CURRENT_DIR, "icons/icon_trash.png"))), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.button_clear_console.setIcon(icon)
        self.button_clear_console.setObjectName(_fromUtf8("button_clear_console"))
        self.button_load_net = QtGui.QPushButton(self.centralwidget)
        self.button_load_net.setGeometry(QtCore.QRect(510, 35, 98, 27))
        self.button_load_net.setObjectName(_fromUtf8("button_load_net"))
        self.textbox_net_file = QtGui.QLineEdit(self.centralwidget)
        self.textbox_net_file.setGeometry(QtCore.QRect(140, 35, 351, 31))
        self.textbox_net_file.setObjectName(_fromUtf8("textbox_net_file"))
        self.textbox_net_file.setText(os.path.join(CURRENT_DIR,'../examples/case1/c1.py'))
        self.lab_net_file = QtGui.QLabel(self.centralwidget)
        self.lab_net_file.setGeometry(QtCore.QRect(50, 10, 60, 81))
        self.lab_net_file.setObjectName(_fromUtf8("lab_net_file"))
        self.lab_algorithm = QtGui.QLabel(self.centralwidget)
        self.lab_algorithm.setGeometry(QtCore.QRect(50, 250, 81, 20))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lab_algorithm.sizePolicy().hasHeightForWidth())
        self.lab_algorithm.setSizePolicy(sizePolicy)
        self.lab_algorithm.setObjectName(_fromUtf8("lab_algorithm"))
        self.lab_routing = QtGui.QLabel(self.centralwidget)
        self.lab_routing.setGeometry(QtCore.QRect(50, 230, 68, 17))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lab_routing.sizePolicy().hasHeightForWidth())
        self.lab_routing.setSizePolicy(sizePolicy)
        self.lab_routing.setObjectName(_fromUtf8("lab_routing"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setEnabled(True)
        self.label.setGeometry(QtCore.QRect(440, 320, 66, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.list_routing = QtGui.QListView(self.centralwidget)
        self.list_routing.setGeometry(QtCore.QRect(140, 210, 351, 81))
        self.list_routing.setObjectName(_fromUtf8("list_routing"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 633, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuAbout = QtGui.QMenu(self.menubar)
        self.menuAbout.setAcceptDrops(True)
        self.menuAbout.setObjectName(_fromUtf8("menuAbout"))
        self.menuActions = QtGui.QMenu(self.menubar)
        self.menuActions.setTearOffEnabled(False)
        self.menuActions.setObjectName(_fromUtf8("menuActions"))
        MainWindow.setMenuBar(self.menubar)
        self.actionAbout_us = QtGui.QAction(MainWindow)
        self.actionAbout_us.setObjectName(_fromUtf8("actionAbout_us"))
        # self.actionManual = QtGui.QAction(MainWindow)
        # self.actionManual.setObjectName(_fromUtf8("actionManual"))
        self.actionCompute_ADR = QtGui.QAction(MainWindow)
        self.actionCompute_ADR.setObjectName(_fromUtf8("actionCompute_ADR"))
        self.actionGet_Mission_Prob = QtGui.QAction(MainWindow)
        self.actionGet_Mission_Prob.setObjectName(_fromUtf8("actionGet_Mission_Prob"))
        #self.actionPlot_ADR_Varying_CFP = QtGui.QAction(MainWindow)
        #self.actionPlot_ADR_Varying_CFP.setObjectName(_fromUtf8("actionPlot_ADR_Varying_CFP"))
        #self.actionNew = QtGui.QAction(MainWindow)
        #self.actionNew.setObjectName(_fromUtf8("actionNew"))/
        self.menuAbout.addAction(self.actionAbout_us)
        #self.menuActions.addAction(self.actionNew)
        self.menuActions.addAction(self.actionCompute_ADR)
        self.menuActions.addAction(self.actionGet_Mission_Prob)
        #self.menuActions.addAction(self.actionPlot_ADR_Varying_CFP)
        self.menubar.addAction(self.menuActions.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())


        self.print_to_log("<b>Welcome to DTN-MC</b>")
        self.textbox_console.setReadOnly(True)
        # Load Routing Algorithm
        self.model_routing = QStandardItemModel()
        for algorithm in ssn_tool.routing.routing_setting.DISPLAY_NAME:
            item = QStandardItem(algorithm)
            item.setCheckable(True)
            self.model_routing.appendRow(item)

        self.list_routing.setModel(self.model_routing)

        #
        self.button_add_traffic.setEnabled(False)
        self.button_gen_model.setEnabled(False)

        # Assign controllers
        self.button_load_net.clicked.connect(self.click_load_net)
        self.button_gen_model.clicked.connect(self.click_gen_model)
        self.button_add_traffic.clicked.connect(self.click_add_traffic)
        self.button_remove_traffic.clicked.connect(self.click_remove_button)
        self.button_clear_console.clicked.connect(self.click_clear_console_button)
        self.actionCompute_ADR.triggered.connect(self.controller.run_compute_adr)
        self.actionGet_Mission_Prob.triggered.connect(self.click_get_mission_prob)
        self.actionAbout_us.triggered.connect(self.click_about_us)

        self.enable = True
        self.temp_dir = mkdtemp()
        self.actionCompute_ADR.setEnabled(False)
        self.actionGet_Mission_Prob.setEnabled(False)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "DTN-MC", None))
        self.lab_traffic.setText(_translate("MainWindow", "TRAFFIC", None))
        self.button_add_traffic.setText(_translate("MainWindow", "Add", None))
        self.button_remove_traffic.setText(_translate("MainWindow", "Remove", None))
        self.button_gen_model.setText(_translate("MainWindow", "Generate Model", None))
        self.button_load_net.setText(_translate("MainWindow", "Load", None))
        self.lab_net_file.setText(_translate("MainWindow", "NET FILE", None))
        self.lab_algorithm.setText(_translate("MainWindow", "ALGORITHM", None))
        self.lab_routing.setText(_translate("MainWindow", "ROUTING", None))
        #self.label.setText(_translate("MainWindow", "TextLabel", None))
        self.menuAbout.setTitle(_translate("MainWindow", "About", None))
        self.menuActions.setTitle(_translate("MainWindow", "Actions", None))
        self.actionAbout_us.setText(_translate("MainWindow", "About us", None))
        #self.actionManual.setText(_translate("MainWindow", "Manual", None))
        self.actionCompute_ADR.setText(_translate("MainWindow", "Get ADR", None))
        self.actionGet_Mission_Prob.setText(_translate("MainWindow", "Get Mission Prob", None))
        #self.actionPlot_ADR_Varying_CFP.setText(_translate("MainWindow", "Plot ADR Varying CFP", None))
        #elf.actionNew.setText(_translate("MainWindow", "New", None))


    def set_controller(self, controller):
        self.controller = controller

    def click_load_net(self):
        self.toogle()
        path = self.textbox_net_file.text()
        self.controller.set_net(path)
        self.toogle()

    def click_add_traffic(self):
        print("click add traffic")
        Dialog = QtGui.QDialog()
        Dialog.setModal(True)
        ui = Ui_Dialog()
        ui.setupUi(Dialog)
        ui.set_gui(self)

        ui.combobox_from.addItems(['Node %d' % n for n in range(self.controller.net["NUM_OF_NODES"])])
        ui.comboBox_to.addItems(['Node %d' % n for n in range(self.controller.net["NUM_OF_NODES"])])
        ui.comboBox_ts.addItems([str(ts) for ts in range(max([c["ts"] for c in self.controller.net["CONTACTS"]]) + 1)])

        Dialog.exec_()

    def click_gen_model(self):
        self.toogle()
        if self.controller.net is not None:
            algorithms = []
            print(self.model_routing.rowCount())
            for i in range(self.model_routing.rowCount()):
                item = self.model_routing.item(i)
                if item.checkState() == QtCore.Qt.Checked:
                    algorithms.append(i)
            if len(algorithms) > 0:
                traffic = []
                for t in [self.list_traffic.item(i) for i in range(self.list_traffic.count())]:
                    traffic.append(t.data(QtCore.Qt.UserRole))
                if len(traffic) > 0:
                    working_dir = self.temp_dir
                    if os.access(os.path.dirname(working_dir), os.W_OK):
                        self.print_running_msg("Generating model.")
                        algorithms = [(ssn_tool.routing.routing_setting.DISPLAY_NAME[i],
                                       ssn_tool.routing.routing_setting.FILE_NAME[i],
                                       ssn_tool.routing.routing_setting.CLASS_NAME[i])
                                      for i in algorithms]
                        self.controller.gen_model(algorithms, traffic, working_dir)
                    else:
                        self.print_error_msg("The given working directory is not valid.")
                else:
                    self.print_error_msg("There must be at least 1 traffic in the network.")
            else:
                self.print_error_msg("It is mandatory to choose at least one routing algorithm.")
        self.toogle()



    def click_remove_button(self):
        for i in sorted([item.row() for item in self.list_traffic.selectedIndexes()],reverse=True):
            self.list_traffic.takeItem(i)

    def click_clear_console_button(self):
        self.textbox_console.clear()

    def add_traffic(self, t):
        item = QListWidgetItem("From: Node %d, To: Node %d, TS: %d" %
                                     (t['from'], t['to'], t['ts']), self.list_traffic)
        item.setData(QtCore.Qt.UserRole, t)

    def click_get_mission_prob(self):
        Dialog = QtGui.QDialog()
        Dialog.setModal(True)
        ui = Ui_Set()
        ui.set_controller(self.controller)
        ui.setupUi(Dialog)
        Dialog.exec_()

    def click_about_us(self):
        QDesktopServices.openUrl(QUrl('http://lcd.efn.unc.edu.ar/sitio/'))

    def print_to_log(self, msg):
        cursor = self.textbox_console.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)
        self.textbox_console.setTextCursor(cursor)

        self.textbox_console.insertHtml(msg + "<br>")
        self.textbox_console.moveCursor(QtGui.QTextCursor.End)
        QCoreApplication.processEvents()


    def print_running_msg(self, msg):
        self.print_to_log("<b style='color:DarkOrange'> [Running] </b>" + " " + msg)

    def print_info_msg(self, msg):
        self.print_to_log("<b style='color:blue'> [Info] </b>" + " " + msg)

    def print_error_msg(self, msg):
        self.print_to_log("<b style='color:red'> [Error] </b>" + " " + msg)

    def print_result_msg(self, msg):
        self.print_to_log("<b style='color:green'> [Result] </b>" + " " + msg )

    def toogle(self):
        self.enable = not self.enable
        self.button_load_net.setEnabled(self.enable )
        self.button_add_traffic.setEnabled(self.enable )
        self.button_remove_traffic.setEnabled(self.enable )
        self.button_gen_model.setEnabled(self.enable )
        self.menuActions.setEnabled(self.enable )
        self.list_routing.setEnabled(self.enable)
        self.button_clear_console.setEnabled(self.enable)
        self.textbox_net_file.setEnabled(self.enable)
        self.menuAbout.setEnabled(self.enable)

# def show_running_gif(gui):
#     gui.loading_gif = QMovie(os.path.join(CURRENT_DIR,'icons/pacman.gif'))
#     gui.label.setMovie(gui.loading_gif)
#     gui.label.show()
#     gui.loading_gif.start()

    def closeEvent(self):

        try:
            shutil.rmtree(self.temp_dir)
        except OSError as exc:
            if exc.errno != errno.ENOENT:  #ENOENT - no such file or directory
                raise  # re-raise exception

if __name__ == "__main__":
    import sys
    from ssn_tool.controller import Controller

    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    controller = Controller(ui)
    ui.set_controller(controller)
    ui.setupUi(MainWindow)
    app.aboutToQuit.connect(ui.closeEvent)
    MainWindow.show()
    sys.exit(app.exec_())

