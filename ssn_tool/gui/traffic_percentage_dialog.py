# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/tarfic_percentage_dialog.ui'
#
# Created: Tue Sep 25 17:13:03 2018
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Set(object):
    def setupUi(self, Set):
        Set.setObjectName(_fromUtf8("Set"))
        Set.setFixedSize(426, 114)
        self.buttonBox = QtGui.QDialogButtonBox(Set)
        self.buttonBox.setGeometry(QtCore.QRect(80, 80, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.label = QtGui.QLabel(Set)
        self.label.setGeometry(QtCore.QRect(10, 10, 291, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.splitter = QtGui.QSplitter(Set)
        self.splitter.setGeometry(QtCore.QRect(190, 40, 113, 27))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.spinBox_traffic_percentage = QtGui.QSpinBox(self.splitter)
        self.spinBox_traffic_percentage.setSuffix(_fromUtf8(""))
        self.spinBox_traffic_percentage.setPrefix(_fromUtf8(""))
        self.spinBox_traffic_percentage.setMaximum(100)
        self.spinBox_traffic_percentage.setProperty("value", 90)
        self.spinBox_traffic_percentage.setObjectName(_fromUtf8("spinBox_traffic_percentage"))

        self.retranslateUi(Set)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Set.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Set.reject)
        QtCore.QMetaObject.connectSlotsByName(Set)

        self.buttonBox.accepted.connect(self.click_accept_button)

    def retranslateUi(self, Set):
        Set.setWindowTitle(_translate("Set", "Compute mission probability", None))
        self.label.setText(_translate("Set", "Set traffic percentage to fullfill the mission", None))

    def set_controller(self, controller):
        self.controller = controller

    def click_accept_button(self):
        self.controller.run_compute_mission_prob(float(self.spinBox_traffic_percentage.value()) / 100)

