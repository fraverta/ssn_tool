# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'traffic_dialog.ui'
#
# Created: Sun Sep 23 13:35:17 2018
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setFixedSize(608, 125)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(260, 90, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.layoutWidget = QtGui.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(250, 40, 121, 29))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_to = QtGui.QLabel(self.layoutWidget)
        self.label_to.setObjectName(_fromUtf8("label_to"))
        self.horizontalLayout_2.addWidget(self.label_to)
        self.comboBox_to = QtGui.QComboBox(self.layoutWidget)
        self.comboBox_to.setObjectName(_fromUtf8("comboBox_to"))
        self.horizontalLayout_2.addWidget(self.comboBox_to)
        self.layoutWidget_2 = QtGui.QWidget(Dialog)
        self.layoutWidget_2.setGeometry(QtCore.QRect(410, 40, 121, 29))
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.layoutWidget_2)
        self.horizontalLayout_4.setMargin(0)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_ts = QtGui.QLabel(self.layoutWidget_2)
        self.label_ts.setObjectName(_fromUtf8("label_ts"))
        self.horizontalLayout_4.addWidget(self.label_ts)
        self.comboBox_ts = QtGui.QComboBox(self.layoutWidget_2)
        self.comboBox_ts.setObjectName(_fromUtf8("comboBox_ts"))
        self.horizontalLayout_4.addWidget(self.comboBox_ts)
        self.widget = QtGui.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(90, 40, 121, 29))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_from = QtGui.QLabel(self.widget)
        self.label_from.setObjectName(_fromUtf8("label_from"))
        self.horizontalLayout.addWidget(self.label_from)
        self.combobox_from = QtGui.QComboBox(self.widget)
        self.combobox_from.setObjectName(_fromUtf8("combobox_from"))
        self.horizontalLayout.addWidget(self.combobox_from)

        self.buttonBox.accepted.connect(self.click_button_ok)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Add Traffic", None))
        self.label_to.setText(_translate("Dialog", "To", None))
        self.label_ts.setText(_translate("Dialog", "TS", None))
        self.label_from.setText(_translate("Dialog", "From", None))

    def set_gui(self, gui):
        self.gui = gui

    def click_button_ok(self):
        print("click button ok")
        f = int(self.combobox_from.currentText()[5:])
        t = int(self.comboBox_to.currentText()[5:])
        ts = int(self.comboBox_ts.currentText())

        if f == t:
            self.gui.print_info_msg("Traffic with the same source and destination is not allowed")
        else:
            exists = False
            for i in range(self.gui.list_traffic.count()):
                item = self.gui.list_traffic.item(i).data(QtCore.Qt.UserRole)
                print(item)
                if item['from'] == f and item['to'] == t and item['ts'] == ts:
                    exists = True
                    self.gui.print_info_msg("Traffic from <i>node %d</i> to <i>node %d</i>"
                                          " generated at <i>ts %d</i> already exists"%(f,t,ts))
                    break
            if not exists:
                self.gui.add_traffic({'from': f, 'to': t, 'ts': ts})

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

