'''

Author: Fernando Raverta
'''

import sys
import operator
from functools import reduce

class Routing:

    def routing(self,state,bundle):
        pass

    def update(self, state, route, bundle):
        pass


class Contact:
    # To generate unique identifier by each contact
    # __identifier = 0

    def __init__(self, f, t, s_time, e_time, data_rate, work, identifier):
        self.source_eid = f
        self.destination_eid = t
        self.s_time = s_time
        self.e_time = e_time
        self.data_rate = data_rate
        self.work = work
        self.capacity = (self.e_time - self.s_time) * self.data_rate # Maximum capacity of a contact, it never changes.
        self.residual_volume = self.capacity # The volume of contact which is still available

        self.id = identifier
        #Contact.__identifier += 1

    def __str__(self):
        # return "(f:%d,t:%d,sT%d,eT:%d,dR:%d)"%(self.f,self.t,self.sTime,self.eTime,self.dataRate)
        return "(f:%d,t:%d,rV:%d)" % (self.source_eid, self.destination_eid, self.residual_volume)

    def __repr__(self):
        return '"%s"'%self


class CP:
    # Contacts = {node_id(int):[Contacts]}
    def __init__(self, contacts):
        self.contacts = contacts

    def get_contact_by_id(self, id):
        for n in self.contacts.keys():
            for c in self.contacts[n]:
                if c.id == id:
                    return c

        print("Error in Contact:getContactById(): Contact Id = %d was not found" % (id))
        sys.exit()

    def get_contacts(self):
        return reduce(operator.concat, self.contacts.values(), [])

    def get_contacts_by_source(self, id):
        return self.contacts[id]

    def __str__(self):
        return "CP: " + str(self.contacts)

    __repr__ = __str__


class Route:
    def __init__(self, hops):
        self.hops = hops
        self.arrival_time = sys.maxsize
        self.from_time = sys.maxsize
        self.to_time = 0
        self.max_volume = 0

    def next_hop(self):
        return self.hops[0].destination_eid

    def next_hop_contact(self):
        return self.hops[0]
    def __str__(self):
        res = ""
        for c in self.hops:
            res += c.__str__() + "-> "
        res = res[0:len(res) - 3]
        return res


class Bundle:
    def __init__(self, f, t, byte_length, ts, sender_id=-1):
        # Bundle protocol fields (set by source node)
        self.source_eid = f
        self.destination_eid = t
        self.byte_length = byte_length
        self.creation_timestamp = ts
        self.ttl = sys.maxsize

        self.hop_count = 0
        self.sender_eid = sender_id
        self.current_node = f

    def __str__(self):
        return "Bundle(f=%d,t=%d,current=%d,sender_id=%d)" % (self.source_eid, self.destination_eid,
                                                              self.current_node ,self.sender_eid)

    def __repr__(self):
        return '"%s"' % self

class State:
    # To generate unique identifier by each state
    __identifier = 0

    @staticmethod
    def reset_identifier():
        State.__identifier = 0

    def __init__(self, cps, incoming_bundles, routed_bundles, ts, childs=None, failed_links=None, working_links=None,
                 limbo=None, delivered=None):
        self.id = State.__identifier
        self.cps = cps  # One contact plan for each node in the network
        # Bundles in network which were received from app or other nodes and require being forwarded in current state
        self.incoming_bundles = incoming_bundles
        # [bundle, ts, first hop contact id] Bundles which are stored in nodes waiting for the next hop contact happen.
        self.routed_bundles = routed_bundles
        self.ts = ts  # The time stamp in which this state could happen.

        # All posible next states of this one.
        self.childs = childs if childs is not None else []
        # Links which were assumed as failed since the parent state to this one.
        self.failed_links = failed_links if failed_links is not None else []
        # Links which work since the parent state to this one.
        self.working_links = working_links if working_links is not None else []
        # Bundles in the network for which no route was found.
        self.limbo = limbo if limbo is not None else []
        # Bundles in the network which have already reached the destination node.
        self.delivered_bundles = delivered if delivered is not None else []

        State.__identifier += 1

    def add_child(self, child):
        self.childs.append(child)

    def __str__(self):
        res = "------\n\t"
        res += "CPs: " + str(self.cps) + "\n\t"
        res += "inBundles: " + str(self.incoming_bundles) + "\n\t"
        res += "rBundles: " + str(self.routed_bundles) + "\n\t"
        res += "delivered: " + str(self.delivered_bundles) + "\n\t"
        res += "limbo: " + str(self.limbo) + "\n\t"
        res += "ts: " + str(self.ts) + "\n\t"
        res += "failed_links: " + str(self.failed_links) + "\n"
        res += "------\n"

        for c in self.childs:
            res += "\t With probability " + str(c[0]) + "\n"
            for l in c[1].__str__().split("\n"):
                res += "\t \t" + l + "\n"

        return res

    def __repr__(self):
        res = "{"
        res += '"inBundles":'  + repr(self.incoming_bundles) + ','
        res += '"rBundles":' + repr(self.routed_bundles) + ','
        res += '"delivered":'  + repr(self.delivered_bundles) + ","
        res += '"limbo": ' + repr(self.limbo) + ","
        res += '"ts":'  + repr(self.ts) + ","
        res += '"failed_links":'   + repr(self.failed_links) + ","
        res += '"childs": ['
        for p,c in self.childs:
            res += '{"p": %f'%p + ',"o":' + c.__repr__() + '},'

        if len(self.childs) > 0:
            res = res[0:len(res)-1]

        res += "]}"
        return res

    def gen_parametric_model(self, net, n_of_states):
        res = "dtmc\n\n"
        res += "const double Pf;\n\n"

        res += "\nmodule DTN\n\n"
        res += "\t s: [0..%d] init 0;\n" % n_of_states
        res += "\t d: [-1..%d] init -1;\n\n" % len(net['TRAFFIC'])

        res += self.to_prism_parametric(net)
        res += "endmodule"
        return res

    def to_prism_parametric(self, net):
        if len(self.childs) > 0:
            res = "\t [] s=%d -> " % self.id
            if len(self.childs) > 1:
                for child in map(lambda c: c[1], self.childs):
                    failed_links_prob = "pow(Pf, %d)"%len(child.failed_links) if len(child.failed_links) > 0 else ''
                    working_links_prob = "1 - pow(Pf, %d)"%len(child.working_links) if len(child.working_links)> 0 else ''
                    if failed_links_prob == '' and working_links_prob == '':
                        child_prob = str(1)
                    elif failed_links_prob == '' and working_links_prob != '':
                        child_prob = working_links_prob
                    elif failed_links_prob != '' and working_links_prob == '':
                        child_prob = failed_links_prob
                    else:
                        child_prob = failed_links_prob + ' * ' + working_links_prob

                    res += "%s: (s'=%d)" % (child_prob, child.id)
                    res += " + " if len(child.childs) > 0 else  " & (d'=%d) + " % (len(child.delivered_bundles))
                res = res[:len(res) - 3] + ";\n"
            else:
                res += "1 : (s'=%d)" % self.childs[0][1].id
                res += ";\n" if len(self.childs[0][1].childs) > 0 else " & (d'=%d);\n" % (
                len(self.childs[0][1].delivered_bundles))

            res += "\n".join(map(lambda s: s[1].to_prism_parametric(net), self.childs))
            return res
        else:
            return ""

    def get_prism_model(self, net, n_of_states, traffic=None):
        if traffic is None:
            traffic=net['TRAFFIC']

        consts = [("c%d"%c,net['CONTACTS'][c]['pf']) for c in range(len(net["CONTACTS"]))]

        res = "dtmc\n\n"
        for c in consts:
            res += "const double %s = %f;\n" %(c[0],c[1])

        res += "\nmodule DTN\n\n"
        res += "\t s: [0..%d] init 0;\n" % n_of_states
        res += "\t d: [-1..%d] init -1;\n\n" % len(traffic)

        res += self.to_prism(net)
        res+= "endmodule"

        return res

    def to_prism(self, net):
        if len(self.childs) > 0:
            res =  "\t [] s=%d -> "%self.id
            if len(self.childs) > 1:
                for child in map(lambda c: c[1], self.childs):

                    #failed_links_prob = ' * '.join([c['pf_var'] for c in [net["CONTACTS"][c] for c in child.failed_links]])
                    #working_links_prob = ' * '.join(['(1-%s)'%(c['pf_var']) for c in [net["CONTACTS"][c] for c in child.working_links]])
                    failed_links_prob = ' * '.join(["c%d"%c for c in child.failed_links])
                    working_links_prob = ' * '.join(['(1-c%d)'%c for c in child.working_links])

                    if failed_links_prob == '' and working_links_prob == '':
                        child_prob = str(1)
                    elif failed_links_prob == '' and working_links_prob != '':
                        child_prob = working_links_prob
                    elif failed_links_prob != '' and working_links_prob == '':
                        child_prob = failed_links_prob
                    else:
                        child_prob =  failed_links_prob + ' * ' + working_links_prob

                    res += "%s: (s'=%d)" %(child_prob, child.id)

                    #res += "pow(p,%d) * pow(1-p,%d) : (s'=%d)"%(len(child.failed_links), len(child.working_links), child.id)
                    res += " + " if len(child.childs) > 0 else  " & (d'=%d) + "%(len(child.delivered_bundles))
                res = res[:len(res) - 3] + ";\n"
            else:
                res += "1 : (s'=%d)" % self.childs[0][1].id
                res += ";\n" if len(self.childs[0][1].childs) > 0 else " & (d'=%d);\n" % (len(self.childs[0][1].delivered_bundles))

            res += "\n".join(map(lambda s: s[1].to_prism(net),self.childs))
            return res
        else:
            return ""



'''
Check is a contact is valid, returns true or false
'''
def contact_is_valid(c, num_of_nodes,pf_is_required=True):
    if type(c) == dict and all(k in c.keys() for k in ['from', 'to', 'ts']) and \
        type(c['from']) == int and \
        type(c['to']) == int and \
        type(c['ts']) == int and \
        0 <= c['from'] < num_of_nodes and \
        0 <= c['to'] < num_of_nodes and \
        c['from'] != c['to'] and \
        c['ts'] >= 0 and \
        (not pf_is_required or
        ('pf' in  c.keys()  and type(c['pf']) == float and 0. <= c['pf'] <= 1.)):
                return True
    return False


def validate_traffic_unit(t, num_of_nodes):
    res = type(t) == dict
    res = res and all(k in t.keys() for k in ['from', 'to', 'ts'])
    res = res and type(t['from']) == int and 0 <= t['from'] < num_of_nodes
    res = res and type(t['to']) == int and 0 <= t['to'] < num_of_nodes
    res = res and type(t['ts']) == int and t['ts'] >= 0
    return res


def validate_traffic_list(list_traffic, num_of_nodes):
    if type(list_traffic) == list:
        for t in list_traffic:
            if not validate_traffic_unit(t, num_of_nodes):
                return False
        return True
    else:
        return False

'''
Get Net from file, if traffic required it returns an error if it does not exist.
returns
    {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}] (,'TRAFFIC':{'from':int,'to':int, 'ts':int})}
'''
def get_net_from_file(path_to_net, traffic_required=False, contact_pf_required=True):
    input = {}
    f = exec(open(path_to_net,'r').read(),input)

    #CHECK INPUT FILE HAS THE REQUIRED FIELDS
    if 'NUM_OF_NODES' in input.keys():
        if type(input['NUM_OF_NODES']) != int or input['NUM_OF_NODES'] <= 1:
            print("[ERROR] NUM_OF_NODES must be an integer greater than 1")
            return {}
        else:
            NUM_OF_NODES = input['NUM_OF_NODES']
    else:
        print("[ERROR] The input network must contain NUM_OF_NODES")
        return {}

    if 'CONTACTS' in input.keys():
        if type(input['CONTACTS']) != list or len(input['CONTACTS']) < 1:
            print("[ERROR] CONTACTS must be a list with at least 1 element")
            return {}
        else:
            # Check if each contact is write in the correct way
            for c in input['CONTACTS']:
                if not contact_is_valid(c, NUM_OF_NODES, contact_pf_required):
                    print("[ERROR] Contact must described for a dict: {'from':int,'to':int,'ts':int (,'pf':float)} where: ")
                    print("\t to, from are different and to,from in [0,NUM_OF_NODES)")
                    print("\t ts >= 0")
                    print("\t pf in [0.,1.] (pay attention to write the dots!)")
                    print("\t %s does not satisfy the above properties." % str(c))
                    return {}

            CONTACTS = input['CONTACTS']
    else:
        print("[ERROR] The input network must contain CONTACTS:[{'from':int,'to':int,'ts':int, 'pf':float}]")
        return {}

    # Traffic is readed if it exists. If traffic_required, it reports an error when traffic does not exist
    if 'TRAFFIC' in input.keys():
        t = input['TRAFFIC']
        if type(t) == dict and validate_traffic_unit(t,NUM_OF_NODES):
                    return {'NUM_OF_NODES': NUM_OF_NODES, 'CONTACTS': CONTACTS, 'TRAFFIC':[t]}
        elif type(t) == list and validate_traffic_list(t, NUM_OF_NODES):
            return {'NUM_OF_NODES': NUM_OF_NODES, 'CONTACTS': CONTACTS, 'TRAFFIC': t}
        else:
            print("[ERROR] TRAFFIC must be a list: [{'from':int,'to':int,'ts':int}] where: ")
            print("\t to, from are different and to,from in [0,NUM_OF_NODES)")
            print("\t ts >= 0")
            return {}


    elif traffic_required:
        print("[ERROR] The input network must contain TRAFFIC:{'from':int,'to':int,'ts':int}")
        return {}
    # End Check

    return {'NUM_OF_NODES':NUM_OF_NODES, 'CONTACTS':CONTACTS}

