'''
This file contains the implementation of CGR
Cite RFC

Author: Fernando Raverta
'''

import sys
from ssn_tool.utils import *

class CGR(Routing):


    def __init__(self):
        self.FORBID_RETURN_TO_SENDER = True

    def routing(self, state, bundle):
        return self.cgr_forward(state['nodes'][bundle.current_node], bundle, state['sim_time'])

    def update(self, state, route, bundle):
        #TODO: Manage different kinds of Voume Awarness Mechanism
        # Take note of this routing decision in order to implement First Hop Volume Awareness Mechanism
        route.next_hop_contact().residual_volume -= bundle.byte_length

    def find_next_best_route_by_bdt(self, cp, rootContact, terminusNode):
        current_contact = rootContact
        final_contact = None
        earliest_final_arrival_time = sys.maxsize
        while True:
            current_neighbors = cp.get_contacts_by_source(current_contact.destination_eid)
            for neighbord in current_neighbors:

                # Fist check if it has to be considered
                if neighbord.e_time <= current_contact.work['arrival_time']:
                    continue  # ignore contact

                # This contact is suppressed/visited, ignore it.
                if neighbord.work['suppressed'] or neighbord.work['visited']:
                    continue

                # compute capacity TODO: Ojo con esto, revisar! Posible fuente de errores.
                if neighbord.work['capacity'] == 0:
                    # In DTNSim, we have the following line but since my point of view it never change(I mean, it isn't updated)
                    # neighbord.work['capacity'] = neighbord.getDataRate() * neighbord.getDuration()
                    neighbord.work['capacity'] = neighbord.capacity

                # calculate the cost of this contact
                if neighbord.s_time < current_contact.work['arrival_time']:
                    arrival_time = current_contact.work['arrival_time']
                else:
                    arrival_time = neighbord.s_time

                # update the cost for this contact
                if arrival_time < neighbord.work['arrival_time']:
                    neighbord.work['arrival_time'] = arrival_time
                    neighbord.work['predecessor'] = current_contact

                    # if this contact reaches the terminusNode consider it as final contact
                    if neighbord.destination_eid == terminusNode:
                        if neighbord.work['arrival_time'] < earliest_final_arrival_time:
                            earliest_final_arrival_time = neighbord.work['arrival_time']
                            final_contact = neighbord

            # end for current_contact neighbors
            current_contact.work['visited'] = True
            # select next (best) contact to move to in next iteration
            next_contact = None
            earliest_arrival_time = sys.maxsize
            for contact in cp.get_contacts():
                # Do not evaluate suppressed or visited contacts
                if contact.work['suppressed'] or contact.work['visited']:
                    continue

                # if the arrival time is worst than the best found so far, ignore
                if contact.work['arrival_time'] > earliest_final_arrival_time:
                    continue

                # Then this might be the best candidate contact
                if contact.work['arrival_time'] < earliest_arrival_time:
                    next_contact = contact
                    earliest_arrival_time = contact.work['arrival_time']

            # end for select next best contact
            if next_contact is None:
                break  # No next contact exit searach

            # update next contact and go with the next iteration
            current_contact = next_contact

        # end While.
        if final_contact is not None:
            route = Route([])
            route.arrival_time = earliest_final_arrival_time
            earliest_end_time = sys.maxsize
            max_capacity = sys.maxsize

            contact = final_contact
            while contact != rootContact:

                # get earliest end time
                if contact.e_time < earliest_end_time:
                    earliest_end_time = contact.e_time

                # get the minimal capacity
                if contact.capacity < max_capacity:
                    max_capacity = contact.capacity

                route.hops.insert(0, contact)
                contact = contact.work['predecessor']

            route.from_time = route.hops[0].s_time
            route.to_time = earliest_end_time
            route.max_volume = max_capacity  # the least volume of route links
            return route
        else:
            return None

    '''
    sourceNode: int
    terminusNode: int
    simTime: double
    '''

    def load_route_list(self, cp, source_node, terminus_node, sim_time, route_list):
        root_contact = Contact(source_node, source_node, 0, 0, 0, {'arrival_time': sim_time}, -1)
        for c in cp.get_contacts():
            c.work = {'arrival_time': sys.maxsize, 'capacity': 0, 'predecessor': None, 'visited': False,
                      'suppressed': False}
            anchor_contact = None
        while True:
            route = self.find_next_best_route_by_bdt(cp, root_contact, terminus_node)
            if route is None:
                break
            first_contact = route.hops[0]
            if anchor_contact is not None:
                if anchor_contact != first_contact:
                    for contact in cp.getContacts():
                        contact.work['arrival_time'] = sys.maxsize
                        contact.work['predecessor'] = None
                        contact.work['visited'] = False
                        if contact.source_eid != source_node:
                            contact.work['suppressed'] = False

                    anchor_contact.work['suppressed'] = True
                    anchor_contact = None
                    continue

            route_list[terminus_node].append(route)
            if route.to_time == first_contact.e_time:
                limit_contact = first_contact
            else:
                anchor_contact = first_contact
                for contact in route.hops:
                    if contact.end_t == route.to_time:
                        limit_contact = contact
                        break
            limit_contact.work['suppressed'] = True
            for contact in cp.get_contacts():
                contact.work['arrival_time'] = sys.maxsize
                contact.work['predecessor'] = None
                contact.work['visited'] = False

    '''
        bundle: Bundle
        simTime: Double
        excludedNodes:[int]
        Rl: {int:[Route]}

        Pn:{int(denotes node id): {'arrival_time':double, 'hop_count': int,
                                   'forfeit_time':double,route}}
    '''

    def identify_proximate_nodes(self, cp, bundle, sim_time, excluded_nodes, route_list):
        if bundle.destination_eid not in route_list.keys():
            route_list[bundle.destination_eid] = []
            self.load_route_list(cp, bundle.current_node, bundle.destination_eid, sim_time, route_list)
        proximate_nodes = {}
        for route in route_list[bundle.destination_eid]:
            # --- Discard Route ---
            if route.to_time <= sim_time:
                continue  # ignore past route
            if route.arrival_time > bundle.ttl:
                continue  # route arrives too late
            # if bundle.getByteLenght() > route.capacity:
            #            continue #not enought capacity
            if bundle.byte_length > route.hops[0].residual_volume:
                continue  # if bundle does not fit in first contact ignore it.
            if route.next_hop() in excluded_nodes:
                continue  # next hop is excluded

            # --- Check if it is the best for the neighbor ---
            if route.next_hop() in proximate_nodes.keys():
                proximate_node = proximate_nodes[route.next_hop()]
                if route.arrival_time < proximate_node['arrival_time']:
                    proximate_node['arrival_time'] = route.arrival_time
                    proximate_node['hop_count'] = len(route.hops)
                    proximate_node['forfeit_ime'] = route.to_time
                    proximate_node['route'] = route

                elif route.arrival_time > proximate_node['arrival_time']:
                    pass

                elif len(route.hops) < proximate_node['hop_count']:
                    proximate_node['arrival_time'] = route.arrivalTime
                    proximate_node['hop_count'] = len(route.hops)
                    proximate_node['forfeit_time'] = route.toTime
                    proximate_node['route'] = route
                elif len(route.hops) > proximate_node['hop_count']:
                    pass

            else:
                proximate_nodes[route.next_hop()] = {'arrival_time': route.arrival_time, 'hop_count': len(route.hops),
                                                     'forfeit_time': route.to_time, 'route': route}

        return proximate_nodes

    def cgr_forward(self, cp, bundle, sim_time):
        # En means excluded nodes
        Rl = {}
        En = []
        if self.FORBID_RETURN_TO_SENDER:
            En.append(bundle.current_node)
        Pn = self.identify_proximate_nodes(cp, bundle, sim_time, En, Rl)
        next_hop = None
        for pn in Pn:
            if next_hop is None:
                next_hop = pn
            elif Pn[pn]['arrival_time'] < Pn[next_hop]['arrival_time']:
                next_hop = pn
            elif Pn[pn]['arrival_time'] > Pn[next_hop]['arrival_time']:
                continue
            elif Pn[pn]['hop_count'] < Pn[next_hop]['hop_count']:
                next_hop = pn
            elif Pn[pn]['hop_count'] > Pn[next_hop]['hop_count']:
                continue
            elif Pn[pn]['route'].hops[0].source_eid < Pn[next_hop]['route'].hops[0].source_eid:
                next_hop = pn

        if next_hop is not None:
            return Pn[next_hop]['route']
        else:
            return None
        '''
        if nextHop is not None:
            print("Route to " + str(nextHop) + ": " + str(Pn[nextHop]) )
        else:
            print("No route is available")
        '''

