'''
This file contains the implementation of Direct Routing
Cite RFC

Author: Fernando Raverta
'''

import sys
from ssn_tool.utils import *

class DirectRouting(Routing):


    def __init__(self):
        pass

    def routing(self, state, bundle):
        #search for the first contact with bundle destination
        cp = state['nodes'][bundle.current_node]
        ts = state['sim_time']
        contacts = cp.get_contacts_by_source(bundle.current_node)
        direct_contacts = [c for c in contacts if c.destination_eid == bundle.destination_eid and ts<=c.s_time]
        if len(direct_contacts) > 0:
            chose_contact = min(direct_contacts, key=lambda x: x.s_time)
            return Route([chose_contact])
        else:
            return None



    def update(self, state, route, bundle):
        pass