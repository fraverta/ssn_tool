import os.path
import ssn_tool.setting
import shlex, subprocess
from ssn_tool.utils import get_net_from_file
from ssn_tool.main import run_analysis

class Controller:
    def __init__(self, gui):
        self.net = None
        self.gui = gui
        self.working_dir = ''
        self.traffic = []
        self.algorithms = []
    '''
        0  - OK
        -1 - File does not exist
    '''

    def set_net(self, path_to_file):
        if os.path.isfile(path_to_file):
            self.net = get_net_from_file(path_to_file, traffic_required=False, contact_pf_required=True)
            self.gui.list_traffic.clear()
            self.gui.button_add_traffic.setEnabled(True)
            self.gui.button_gen_model.setEnabled(True)
            if "TRAFFIC" in self.net.keys():
                for t in self.net["TRAFFIC"]:
                    self.gui.add_traffic(t)

        else:
            return -1

    def gen_model(self, algorithms, traffic, working_dir):
        self.algorithms = algorithms
        for display_name, routing_file_name, routing_name in algorithms:
            if run_analysis(self.net, routing_file_name,
                            routing_name, working_dir, traffic=traffic, routing_parameters='') == 0:

                self.gui.print_info_msg("Model for routing algorithm <i>%s</i> "
                                      "was generated succesfully" % display_name)
            else:
                self.gui.print_error_msg("An unexpected error has been found while "
                                    "running model for <i>%s</i>" % display_name)
                return -1

        self.gui.actionCompute_ADR.setEnabled(True)
        self.gui.actionGet_Mission_Prob.setEnabled(True)
        #self.gui.actionPlot_ADR_Varying_CFP.setEnabled(True)

        self.working_dir = working_dir
        self.traffic = traffic

        return 0

    def run_compute_adr(self):
        self.gui.toogle()
        algorithms_average_del_ratio = []
        for algorithm in self.algorithms:
            path_to_model = os.path.join(self.working_dir, 'model-%s.prism'%algorithm[2])
            if os.path.isfile(path_to_model):
                self.gui.print_running_msg("Computing Average Delivery Ratio "
                                            "for <i>%s</i>. It could take a long while."%algorithm[0])
                try:
                    average_del_ratio = 0
                    for t in range(1, len(self.traffic) + 1):
                        cmd = shlex.split("%s %s  -pf 'P=?[F d=%d]'" % (ssn_tool.setting.PATH_TO_PRISM, path_to_model, t))
                        output = subprocess.check_output(cmd)
                        res = next(x for x in output.splitlines() if x.startswith(b"Result:")).decode("utf-8")
                        average_del_ratio += float(res.split(' ')[1]) * t

                    average_del_ratio /= len(self.traffic)
                    self.gui.print_result_msg("The average delivery ratio for "
                                          "<i>%s</i> is <b>%f</b>"% (algorithm[0],average_del_ratio))
                    algorithms_average_del_ratio.append(average_del_ratio)
                except Exception as e:
                    self.gui.print_error_msg("An unexpected error has happend while "
                                          "running PRISM. Check if the path has been set correctly in file: "
                                          "ssn_tool/setting.py")

                    print(str(e))
                    break
            else:
                self.gui.print_error_msg("An unexpected error has happend. Try generating the model again.")

        self.gui.toogle()

    def run_compute_mission_prob(self, traffic_proportion):
        print(str(traffic_proportion))
        self.gui.toogle()
        algorithms_average_del_ratio = []
        for algorithm in self.algorithms:
            path_to_model = os.path.join(self.working_dir, 'model-%s.prism' % algorithm[2])
            if os.path.isfile(path_to_model):
                self.gui.print_running_msg("Computing Average Delivery Ratio "
                                           "for <i>%s</i>. It could take a long while." % algorithm[0])
                try:
                    cmd = shlex.split("%s %s  -pf 'P=?[F d>=%f]'" % (ssn_tool.setting.PATH_TO_PRISM, path_to_model,
                                                                     traffic_proportion * len(self.traffic)))
                    output = subprocess.check_output(cmd)
                    res = next(x for x in output.splitlines() if x.startswith(b"Result:")).decode("utf-8")
                    fullfil_mission_probability = float(res.split(' ')[1])
                    self.gui.print_result_msg("The probability of fulfilling mission of delivering %.2f"
                                              "%% of the given traffic using <i>%s</i>  is "
                                              "<b>%f</b>" % (traffic_proportion * 100, algorithm[0], fullfil_mission_probability))
                except Exception as e:
                    self.gui.print_error_msg("An unexpected error has happened while "
                                             "running PRISM. Check if the path has been set correctly in file: "
                                             "ssn_tool/setting.py")

                    print(str(e))
                    break
            else:
                self.gui.print_error_msg("An unexpected error has happened. Try generating the model again.")

        self.gui.toogle()

class DemoGui:
    def add_traffic(self, t):
        pass

    def print_to_log(self, msg):
        print(msg)


# controller = Controller(DemoGui)
# controller.set_net('/home/nando/Desktop/SSC/ssn_tool/examples/case1/c1.py')
# controller.traffic = controller.net["TRAFFIC"]
# controller.working_dir = '/home/nando/Desktop/borrar'
# print(controller.run_compute_adr())
