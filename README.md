# DTN-MC

This is the repository for DTN-MC application presented at School on Systems and Networks, Valdivia - Chile in 2018. 
Watchis this video https://youtu.be/-7x96hDcy3I and read the files paper.pdf and poster.pdf for a quick overview of the tool.

To run python3 main.py
Previously you should set the path to PRISM in ssn_tool/setting.py
It is necessary to have PyQt4. To intall it in Ubuntu-Linux execute the following command: sudo apt-get install python3-pyqt4

Contact us: fdraverta@gmail.com

Thanks for visiting,  
Fernando Raverta

