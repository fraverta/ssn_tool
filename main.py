import sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ssn_tool.controller import Controller
from ssn_tool.gui.MainWindow import Ui_MainWindow

app = QtGui.QApplication(sys.argv)
MainWindow = QtGui.QMainWindow()
ui = Ui_MainWindow()
controller = Controller(ui)
ui.set_controller(controller)
ui.setupUi(MainWindow)
app.aboutToQuit.connect(ui.closeEvent)
MainWindow.show()
sys.exit(app.exec_())

